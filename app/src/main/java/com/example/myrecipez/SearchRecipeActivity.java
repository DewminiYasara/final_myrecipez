package com.example.myrecipez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SearchRecipeActivity extends AppCompatActivity {

    public static ArrayList<Recipe> recipes = new ArrayList<>();

    String[] titleArray;
    String[] imageArray;

    ListView recipeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipe);

        recipes.clear();

        findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(v);
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SearchRecipeActivity.this,MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });
    }

    public void search(View view){
        recipes.clear();
        EditText search = findViewById(R.id.et_search);
        String url = "http://www.recipepuppy.com/api/?q="+search.getText().toString();
        url = url.replace(" ","%20");
        Ion.with(this)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if(result.equals(null))
                        {
                            TextView txt = findViewById(R.id.txt_noResults);
                            txt.setText("No results");
                        }else{
                            displayResults(result);
                        }
                    }
                });
    }

    public void displayResults(String result){
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray results = jsonObject.getJSONArray("results");
            for(int i=0;i<results.length();i++){
                Recipe recipe = new Recipe(results.getJSONObject(i));
                recipes.add(recipe);
                recipe.setRecipeId(i);
            }
            titleArray = new String[recipes.size()];
            imageArray = new String[recipes.size()];

            for(int i=0;i<recipes.size();i++){
                titleArray[i] = recipes.get(i).getTitle();
                imageArray[i] = recipes.get(i).getThumbnail();
            }

            CustomListAdapter customListAdapter = new CustomListAdapter(this,titleArray,imageArray);
            recipeList = findViewById(R.id.recipeList);
            recipeList.setAdapter(customListAdapter);
            recipeList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    Intent recipeDetailsIntent = new Intent(SearchRecipeActivity.this,RecipeDetailsActivity.class);
                    recipeDetailsIntent.putExtra("id",position);
                    startActivity(recipeDetailsIntent);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
