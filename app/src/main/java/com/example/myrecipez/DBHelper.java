package com.example.myrecipez;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "savedRecipes.db";
    public static final String TABLE_NAME = "recipes";
    public static final String COL_ID ="id";
    public static final String COL_HREF ="href";
    public static final String COL_INGREDIENTS = "ingredients";
    public static final String COL_TITLE = "title";
    public static final String COL_THUMBNAIL = "thumbnail";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_HREF + " TEXT," +
                COL_INGREDIENTS + " TEXT," +
                COL_TITLE + " TEXT," +
                COL_THUMBNAIL+ " TEXT" + " );"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String href,String ingredients,String title,String thumbnail)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_HREF,href);
        contentValues.put(COL_INGREDIENTS,ingredients);
        contentValues.put(COL_TITLE,title);
        contentValues.put(COL_THUMBNAIL, thumbnail);

        long result = db.insert(TABLE_NAME, null, contentValues);
        db.close();
        if(result==-1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

     public ArrayList<Recipe> queryAllData()
     {
         ArrayList<Recipe> recipes = new ArrayList<>();
         SQLiteDatabase db = this.getReadableDatabase();
         //String[] columns = {COL_ID,COL_HREF,COL_INGREDIENTS,COL_THUMBNAIL,COL_TITLE};
         Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);

         if(cursor.moveToFirst()){
             do{
                 Recipe recipe = new Recipe();
                 recipe.setRecipeId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                 recipe.setHref(cursor.getString(cursor.getColumnIndex(COL_HREF)));
                 recipe.setIngredients(cursor.getString(cursor.getColumnIndex(COL_INGREDIENTS)));
                 recipe.setTitle(cursor.getString(cursor.getColumnIndex(COL_TITLE)));
                 recipe.setThumbnail(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIL)));
                 recipes.add(recipe);
             }while (cursor.moveToNext());
         }
         cursor.close();
         db.close();
         return recipes;
     }

     public boolean updateData(int id,String ingredients)
     {
         SQLiteDatabase db = this.getWritableDatabase();
         //db.execSQL("UPDATE "+TABLE_NAME+" SET "+COL_INGREDIENTS+"="+ingredients+" WHERE "+COL_ID+"="+id+";");
         ContentValues contentValues = new ContentValues();
         contentValues.put(COL_INGREDIENTS,ingredients);
         db.update(TABLE_NAME,contentValues,COL_ID+"="+id,null);
         db.close();
         return true;
     }

    public boolean deleteData(int id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        //db.execSQL("DELETE "+TABLE_NAME+" WHERE "+COL_ID+"="+id);
        db.delete(TABLE_NAME,COL_ID+"="+id,null);
        db.close();
        return true;
    }
}
