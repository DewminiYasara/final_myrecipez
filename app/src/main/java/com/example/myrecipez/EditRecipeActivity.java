package com.example.myrecipez;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class EditRecipeActivity extends AppCompatActivity {

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id",0);

        final Recipe recipe = SavedRecipesActivity.recipes.get(id);

        dbHelper = new DBHelper(this);

        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(recipe.getTitle());

        final EditText etIngredients = findViewById(R.id.et_ingredients);
        etIngredients.setText(recipe.getIngredients());
        etIngredients.setEnabled(false);

        final Button btnSave = findViewById(R.id.btn_save);
        final Button btnEdit = findViewById(R.id.btn_edit);
        final Button btnRemove = findViewById(R.id.btn_remove);
        btnSave.setEnabled(false);

        if (!TextUtils.isEmpty(recipe.getThumbnail())) {
            Picasso.get()
                    .load(recipe.getThumbnail())
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into((ImageView)findViewById(R.id.im_recipe));
        } else {
            Picasso.get().load(R.drawable.icon1).into((ImageView)findViewById(R.id.im_recipe));
        }

        findViewById(R.id.btn_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSave.setEnabled(true);
                btnEdit.setEnabled(false);
                btnRemove.setEnabled(false);
                etIngredients.setEnabled(true);
            }
        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dbHelper.updateData(recipe.getRecipeId(),etIngredients.getText().toString())){
                    Toast.makeText(EditRecipeActivity.this,"Recipe modified!",Toast.LENGTH_SHORT).show();
                    etIngredients.setEnabled(false);
                    btnSave.setEnabled(false);
                    btnEdit.setEnabled(true);
                    btnRemove.setEnabled(true);
                }else{
                    Toast.makeText(EditRecipeActivity.this,"Failed to modify recipe!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btn_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditRecipeActivity.this);
                builder.setTitle("Remove Recipe");
                builder.setMessage("Are you sure you want to delete this recipe?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(dbHelper.deleteData(recipe.getRecipeId())){
                            Toast.makeText(EditRecipeActivity.this,"Recipe deleted!",Toast.LENGTH_SHORT).show();
                            Intent savedRecipesIntent = new Intent(EditRecipeActivity.this,SavedRecipesActivity.class);
                            startActivity(savedRecipesIntent);
                            finish();
                        }else {
                            Toast.makeText(EditRecipeActivity.this,"Failed to delete recipe!",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        findViewById(R.id.btn_savedList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent savedRecipesIntent = new Intent(EditRecipeActivity.this,SavedRecipesActivity.class);
                startActivity(savedRecipesIntent);
                finish();
            }
        });

        findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(EditRecipeActivity.this,MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        findViewById(R.id.btn_visit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String href = recipe.getHref();
                Intent visitIntent = new Intent(Intent.ACTION_VIEW);
                visitIntent.setData(Uri.parse(href));
                startActivity(visitIntent);
            }
        });
    }
}
