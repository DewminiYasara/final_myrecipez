package com.example.myrecipez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class SavedRecipesActivity extends AppCompatActivity {

    public static ArrayList<Recipe> recipes = new ArrayList<>();

    String[] titleArray;
    String[] imageArray;

    DBHelper dbHelper;

    ListView recipeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_recipes);

        recipes.clear();

        findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SavedRecipesActivity.this,MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        dbHelper = new DBHelper(this);
        recipes = dbHelper.queryAllData();

        titleArray = new String[recipes.size()];
        imageArray = new String[recipes.size()];

        for(int i=0;i<recipes.size();i++){
            titleArray[i] = recipes.get(i).getTitle();
            imageArray[i] = recipes.get(i).getThumbnail();
        }

        CustomListAdapter customListAdapter = new CustomListAdapter(this,titleArray,imageArray);
        recipeList = findViewById(R.id.recipeList);
        recipeList.setAdapter(customListAdapter);
        recipeList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                Intent recipeDetailsIntent = new Intent(SavedRecipesActivity.this,EditRecipeActivity.class);
                recipeDetailsIntent.putExtra("id",position);
                startActivity(recipeDetailsIntent);
                finish();
            }
        });
    }
}
