package com.example.myrecipez;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

public class CustomListAdapter extends ArrayAdapter {

    private final Activity context;
    private final String[] imageArray;
    private final String[] titleArray;

    public CustomListAdapter(Activity context,String[] titleArrayParam,String[] imageArrayParam)
    {
        super(context,R.layout.listview_row,titleArrayParam);
        this.context=context;
        this.imageArray = imageArrayParam;
        this.titleArray = titleArrayParam;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listview_row,null,true);
        TextView txtTitle = rowView.findViewById(R.id.txt_title);
        txtTitle.setText(titleArray[position]);
        ImageView imageIcon = rowView.findViewById(R.id.im_icon);
        if (!TextUtils.isEmpty(imageArray[position])) {
            Picasso.get()
                    .load(imageArray[position])
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(imageIcon);
        } else {
            Picasso.get().load(R.drawable.icon1).into(imageIcon);
        }
        return rowView;
    }
}
