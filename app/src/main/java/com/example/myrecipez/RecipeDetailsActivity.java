package com.example.myrecipez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class RecipeDetailsActivity extends AppCompatActivity {

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);

        dbHelper = new DBHelper(this);

        final Intent intent = getIntent();
        int id = intent.getIntExtra("id",0);

        final Recipe recipe = SearchRecipeActivity.recipes.get(id);

        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(recipe.getTitle());

        TextView txtIngredients = findViewById(R.id.txt_ingredients);
        txtIngredients.setText(recipe.getIngredients());

        if (!TextUtils.isEmpty(recipe.getThumbnail())) {
            Picasso.get()
                    .load(recipe.getThumbnail())
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into((ImageView)findViewById(R.id.im_recipe));
        } else {
           Picasso.get().load(R.drawable.icon1).into((ImageView)findViewById(R.id.im_recipe));
        }

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.btn_savedRecipes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent savedRecipesIntent = new Intent(RecipeDetailsActivity.this,SavedRecipesActivity.class);
                startActivity(savedRecipesIntent);
                finish();
            }
        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dbHelper.insertData(recipe.getHref(),recipe.getIngredients(),recipe.getTitle(),recipe.getThumbnail())){
                    Toast.makeText(RecipeDetailsActivity.this,"Recipe saved!",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(RecipeDetailsActivity.this,"Failed to save recipe!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btn_visit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String href = recipe.getHref();
                Intent visitIntent = new Intent(Intent.ACTION_VIEW);
                visitIntent.setData(Uri.parse(href));
                startActivity(visitIntent);
            }
        });
    }
}
