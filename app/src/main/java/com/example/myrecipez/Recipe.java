package com.example.myrecipez;

import org.json.JSONException;
import org.json.JSONObject;

public class Recipe {

    private int recipeId;
    private String title;
    private String href;
    private String ingredients;
    private String thumbnail;

    public Recipe(){

    }

    public Recipe(JSONObject recipe){
        try {
            this.title = recipe.getString("title");
            this.href = recipe.getString("href");
            this.ingredients = recipe.getString("ingredients");
            this.thumbnail = recipe.getString("thumbnail");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public int getRecipeId(){
        return this.recipeId;
    }

    public String getTitle(){
        return this.title;
    }

    public String getHref(){
        return this.href;
    }

    public String getIngredients(){
        return this.ingredients;
    }

    public String getThumbnail(){
        return this.thumbnail;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setHref(String href){
        this.href = href;
    }

    public void setIngredients(String ingredients){
        this.ingredients = ingredients;
    }

    public void setThumbnail(String thumbnail){
        this.thumbnail = thumbnail;
    }

    public void setRecipeId(int recipeId){
        this.recipeId = recipeId;
    }
}
